﻿namespace Layers.Users.Types
{
	public interface IUserManager
	{
		IUserViewModel Create(IUserCreateModel user);
		IUserViewModel Retrieve(int userID);
		IUserViewModel Update(IUserUpdateModel user);
		void Delete(int userID);
	}
}
