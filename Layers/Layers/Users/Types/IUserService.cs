﻿namespace Layers.Users.Types
{
	public interface IUserService
	{
		IUserViewModel Create(IUserCreateModel userCreateModel);
		IUserViewModel Retrieve(int userID);
		IUserViewModel Update(IUserUpdateModel userUpdateModel);
		void Delete(int userID);
	}
}
