﻿namespace Layers.Users.Types
{
	public interface IUserRepository
	{
		void Create(IUser user);
		IUser Retrieve(int userID);
		void Update(IUser user);
		void Delete(int userID);
	}
}
