﻿namespace Layers.Users.Types
{
	public interface IUserFactory
	{
		IUser Convert(IUserCreateModel createModel);
		IUserViewModel Convert(IUser user);
	}
}
