﻿using Layers.Users.Types;

namespace Layers.Users
{
	public class UserService : IUserService
	{
		private readonly IUserFactory userFactory;
		private readonly IUserRepository userRepository;

		public UserService(IUserFactory userFactory, IUserRepository userRepository)
		{
			this.userFactory = userFactory;
			this.userRepository = userRepository;
		}

		public IUserViewModel Create(IUserCreateModel userCreateModel)
		{
			IUser user = userFactory.Convert(userCreateModel);
			userRepository.Create(user); // will create and fill generated fields (ID)
			return userFactory.Convert(user);
		}

		public void Delete(int userID)
		{
			userRepository.Delete(userID);
		}

		public IUserViewModel Retrieve(int userID)
		{
			return userFactory.Convert(userRepository.Retrieve(userID));
		}

		public IUserViewModel Update(IUserUpdateModel userUpdateModel)
		{
			IUser user = userRepository.Retrieve(0); // userUpdateModel.ID here

			// user.FirstName = userUpdateModel.FirstName

			userRepository.Update(user);

			return userFactory.Convert(user);
		}
	}
}
