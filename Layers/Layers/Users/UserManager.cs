﻿using Layers.Reminders.Types;
using Layers.Users.Types;

namespace Layers.Users
{
	public class UserManager : IUserManager
	{
		private readonly IUserService userService;
		private readonly IReminderService reminderService;

		public UserManager(IUserService userService)
		{
			this.userService = userService;
		}

		// 2. A new business rule says we should create a reminder for a user when they're created to complete their user profile
		//       we also don't want them to have a notification for the reminder since it will be their first time logging in and it isn't necessary
		public IUserViewModel Create(IUserCreateModel user)
		{
			IUserViewModel viewModel = userService.Create(user);

			reminderService.Create(null); // the new reminder for them

			// In doing this, we have avoided the specific business rule 1 for creating reminders and notifying the user
			//    that we didn't want to do since we know they wont need to refresh their reminder list to see the one we made here
			//    (think about how our notifications work today where we pull a list of notifications, and websocket notification is to tell
			//     the client to fetch their new notifications)

			return viewModel;
		}

		public void Delete(int userID)
		{
			userService.Delete(userID);
		}

		public IUserViewModel Retrieve(int userID)
		{
			return userService.Retrieve(userID);
		}

		public IUserViewModel Update(IUserUpdateModel user)
		{
			return userService.Update(user);
		}
	}
}
