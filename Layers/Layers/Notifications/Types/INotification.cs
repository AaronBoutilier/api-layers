﻿namespace Layers.Notifications.Types
{
	public interface INotification
	{
		int[] UserIDs { get; set; }
		string Payload { get; set; }
	}
}
