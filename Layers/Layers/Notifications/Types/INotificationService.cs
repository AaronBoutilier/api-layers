﻿namespace Layers.Notifications.Types
{
	public interface INotificationService
	{
		void Send(INotification notification);
	}
}
