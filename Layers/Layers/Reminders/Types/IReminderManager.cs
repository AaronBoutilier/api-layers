﻿namespace Layers.Reminders.Types
{
	public interface IReminderManager
	{
		IReminderViewModel Create(IReminderCreateModel createModel);
		IReminderViewModel Retrieve(int id);
		IReminderViewModel Update(IReminderUpdateModel updateModel);
		void Delete(int id);
	}
}
