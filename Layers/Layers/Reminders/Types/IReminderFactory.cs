﻿namespace Layers.Reminders.Types
{
	public interface IReminderFactory
	{
		IReminder Convert(IReminderCreateModel createModel);
		IReminderViewModel Convert(IReminder reminder);
	}
}
