﻿namespace Layers.Reminders.Types
{
	public interface IReminderCreateModel
	{
		int CreatedByUserID { get; set; }
		int UserIDToRemind { get; set; }
	}
}
