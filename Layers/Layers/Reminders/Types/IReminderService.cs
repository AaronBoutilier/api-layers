﻿namespace Layers.Reminders.Types
{
	public interface IReminderService
	{
		IReminderViewModel Create(IReminderCreateModel createModel);
		IReminderViewModel Retrieve(int id);
		IReminderViewModel Update(IReminderUpdateModel updateModel);
		void Delete(int id);
	}
}
