﻿namespace Layers.Reminders.Types
{
	public interface IReminderRepository
	{
		void Create(IReminder reminder);
		IReminder Retrieve(int id);
		void Update(IReminder reminder);
		void Delete(int id);
	}
}
