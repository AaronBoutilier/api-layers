﻿using Layers.Reminders.Types;

namespace Layers.Reminders
{
	public class ReminderService : IReminderService
	{
		private readonly IReminderFactory ReminderFactory;
		private readonly IReminderRepository ReminderRepository;

		public ReminderService(IReminderFactory ReminderFactory, IReminderRepository ReminderRepository)
		{
			this.ReminderFactory = ReminderFactory;
			this.ReminderRepository = ReminderRepository;
		}

		public IReminderViewModel Create(IReminderCreateModel ReminderCreateModel)
		{
			IReminder Reminder = ReminderFactory.Convert(ReminderCreateModel);
			ReminderRepository.Create(Reminder); // will create and fill generated fields (ID)
			return ReminderFactory.Convert(Reminder);
		}

		public void Delete(int ReminderID)
		{
			ReminderRepository.Delete(ReminderID);
		}

		public IReminderViewModel Retrieve(int ReminderID)
		{
			return ReminderFactory.Convert(ReminderRepository.Retrieve(ReminderID));
		}

		public IReminderViewModel Update(IReminderUpdateModel ReminderUpdateModel)
		{
			IReminder Reminder = ReminderRepository.Retrieve(0); // ReminderUpdateModel.ID here

			// Reminder.FirstName = ReminderUpdateModel.FirstName

			ReminderRepository.Update(Reminder);

			return ReminderFactory.Convert(Reminder);
		}
	}
}
