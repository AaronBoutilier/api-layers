﻿using Layers.Notifications.Types;
using Layers.Reminders.Types;

namespace Layers.Reminders
{
	public class ReminderManager : IReminderManager
	{
		private readonly IReminderService reminderService;
		private readonly INotificationService notificationService;

		public ReminderManager(IReminderService reminderService, INotificationService notificationService)
		{
			this.reminderService = reminderService;
			this.notificationService = notificationService;
		}

		// 1. new business rules requires us to notify users when a new reminder is created for them, but not if it was made by them.
		public IReminderViewModel Create(IReminderCreateModel reminder)
		{
			IReminderViewModel viewModel = reminderService.Create(reminder);

			if (reminder.CreatedByUserID != reminder.UserIDToRemind)
				notificationService.Send(null);

			return viewModel;
		}

		public void Delete(int id)
		{
			reminderService.Delete(id);
		}

		public IReminderViewModel Retrieve(int id)
		{
			return reminderService.Retrieve(id);
		}

		public IReminderViewModel Update(IReminderUpdateModel reminder)
		{
			return reminderService.Update(reminder);
		}
	}
}
